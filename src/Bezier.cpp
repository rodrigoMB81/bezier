#include "Bezier.h"

Bezier::Bezier() : poligonoDeControl(sf::LinesStrip, 0), segmentos(100), curva(sf::LinesStrip, 0){
	incremento = 1.0 / segmentos;
	t = 0;
	
}

Bezier::Bezier(sf::Color newColor) : poligonoDeControl(sf::LinesStrip, 0), segmentos(100), curva(sf::LinesStrip, 0){
	color = newColor;
	incremento = 1.0 / segmentos;
	t = 0;
}

Bezier::~Bezier() {
	
}

void Bezier::agregarPunto(sf::Vector2f punto){
	puntosDeControl.push_back(punto);
	poligonoDeControl.append(punto);
	grado = (float)puntosDeControl.size() - 1.f;
	
	if(puntosDeControl.size() > 2)
		calcularPuntos();
//	std::cout<<std::endl;
//	for(int i = 0; i < puntosDeControl.size(); ++i){
//		std::cout<<std::endl<<"x: "<<puntosDeControl[i].x;
//		std::cout<<std::endl<<"y: "<<puntosDeControl[i].y;
//	}
	//calcularPuntos();
}

void Bezier::quitarUltimoPunto(){
	puntosDeControl.pop_back();
	poligonoDeControl.clear();
}

void Bezier::calcularPuntos() {
	curva.clear();
	//sf::Vector2f B = poligonoDeControl[0].position;
	float x;
	float y;
	t = 0;
	for(int j = 0; j <= segmentos; ++j){
		x = 0;
		y = 0;
		for(int i = 0; i < grado + 1; ++i){
			float coeficiente = (factorial(grado) / (factorial(i)*factorial(grado-i)));
			float potenciaUno = pow((1.f - t), grado-(float)i);
			float potenciaDos = pow(t, (float)i);
			x += coeficiente * puntosDeControl[i].x * potenciaUno * potenciaDos;
			y += coeficiente * puntosDeControl[i].y * potenciaUno * potenciaDos;
			//std::cout<<std::endl<<"coef: "<<coeficiente;

		}
		//debug code
//		std::cout<<std::endl<<"t: "<<t;
//		std::cout<<std::endl<<"x: "<<x;
//		std::cout<<std::endl<<"y: "<<y;
		curva.append(sf::Vector2f(x, y));
		t = t + incremento;
	}
	setColor(color);
}

int Bezier::factorial(int arg){
	int resultado = arg;
	for(int i = 1; i < arg; ++i){
		resultado = resultado * i;
	}
	if(resultado == 0)
		return 1.f;
	else
		return resultado;
}

void Bezier::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	
	target.draw(poligonoDeControl);
	target.draw(curva);
}

void Bezier::setColor(sf::Color color) {
	for(int i = 0; i < segmentos; ++i){
		curva[i].color = color;
	}
}
