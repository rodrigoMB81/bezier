#include <SFML/Graphics.hpp>
#include <memory>
#include <cmath>
#include "Bezier.h"
using namespace sf;

void interpretarEntrada(Mouse::Button, bool, Bezier&);
RenderWindow w(VideoMode(640,480),"Ejemplo de SFML");
int main(int argc, char *argv[]){
	
	Bezier miPrimerCurva(Color::Yellow);
	
	while(w.isOpen()) {
		Event e;
		while(w.pollEvent(e)) {
			switch(e.type){
				case(Event::Closed):
					w.close();
					break;
				case(Event::MouseButtonReleased):
					interpretarEntrada(e.mouseButton.button, true, miPrimerCurva);
					break;
			}
		}
		//miPrimerCurva.calcularPuntos();
		w.clear(Color(144, 0, 245, 255));
		w.draw(miPrimerCurva);
		w.display();
	}
	return 0;
}


void interpretarEntrada(Mouse::Button boton, bool fueLiberado, Bezier& curva){
	if(fueLiberado){
		curva.agregarPunto(Vector2f(Mouse::getPosition(w)));

//		std::cout<<std::endl<<"mouse x: "<<Mouse::getPosition(w).x;
//		std::cout<<std::endl<<"mouse y: "<<Mouse::getPosition(w).y;
	}
}
