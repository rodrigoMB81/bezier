#ifndef BEZIER_H
#define BEZIER_H
#include <memory>
#include <SFML/Graphics.hpp>
#include <cmath>
using namespace sf;

class Bezier : public sf::Drawable {
private:
	std::vector<sf::Vector2f> puntosDeControl;
	sf::VertexArray poligonoDeControl;
	sf::VertexArray curva;
	int grado;
	float incremento;
	float segmentos;
	sf::Color color;	
	int factorial(int);
	void draw(sf::RenderTarget&, sf::RenderStates) const;
	float t;
	
public:
	Bezier();
	Bezier(sf::Color);
	~Bezier();
	void agregarPunto(sf::Vector2f);
	void quitarUltimoPunto();
	void setColor(sf::Color);
	void calcularPuntos();
	
	
};

#endif

